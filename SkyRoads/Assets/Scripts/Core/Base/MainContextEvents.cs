﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Events {

    None,
    //ui

    UIMainScreenLoad,
    UIMainScreenRemove,

    //uiend

    SFXOnVolumeChanged,
    BGMusicLoad,
    GameLoad,
    GameRemove,
    AppBack,

    GameLoadLevel,
    GameStart,
    GameRestart,
    GameEnd,
    ShipHit,
    AddScore,
    ResetSpawner,
    BoostEnabled,

    End
}
