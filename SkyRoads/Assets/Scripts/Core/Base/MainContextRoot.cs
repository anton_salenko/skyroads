﻿using strange.extensions.command.api;
using strange.extensions.command.impl;
using strange.extensions.context.api;
using strange.extensions.context.impl;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainContextRoot : MVCSContext
{

    public MainContextRoot(MonoBehaviour view) : base(view)
    {

    }

    public MainContextRoot(MonoBehaviour view, ContextStartupFlags flags) : base(view, flags)
    {
    }

    protected override void mapBindings()
    {

        
        commandBinder.Bind(ContextEvent.START).To<AppStartCommand>().Once().Pooled();

        // ui
        mediationBinder.BindView<UIMainScreenView>().ToMediator<UIMainScreenMediator>();


        commandBinder.Bind(Events.UIMainScreenLoad).To<UIMainScreenLoadCommand>();
        commandBinder.Bind(Events.UIMainScreenRemove).To<UIMainScreenRemoveCommand>();

        // ui end
        mediationBinder.BindView<SFXSoundVolumeBehaviour>().ToMediator<SFXSoundVolumeMediator>();
        mediationBinder.BindView<AsteroidsPool>().ToMediator<AsteroidsPoolMediator>();
        mediationBinder.BindView<GroundPool>().ToMediator<GroundPoolMediator>();
        mediationBinder.BindView<GameView>().ToMediator<GameMediator>();
        mediationBinder.BindView<ScoreCalculatorView>().ToMediator<ScoreCalculatorMediator>();
        mediationBinder.BindView<ShipView>().ToMediator<ShipMediator>();
        mediationBinder.BindView<ObjectSpawner>().ToMediator<ObjectSpawnerMediator>();
        commandBinder.Bind(Events.BGMusicLoad).To<BGMusicLoadCommand>();
        commandBinder.Bind(Events.GameLoad).To<GameLoadCommand>();
    }

    protected override void addCoreComponents()
    {
        base.addCoreComponents();
        injectionBinder.Unbind<ICommandBinder>(); //Unbind to avoid a conflict!
        injectionBinder.Bind<IExecutor>().To<Executor>().ToSingleton();
        injectionBinder.Bind<ICommandBinder>().To<EventCommandBinder>().ToSingleton();
        injectionBinder.Bind<IAsteroidSpawner>().To<AsteroidSpawnRandomService>();
        injectionBinder.Bind<IScoreSave>().To<PlayerPrefsScoreSaveService>();
        injectionBinder.Bind<SFXManager>().ToSingleton();


    }
}
