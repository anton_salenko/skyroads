﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IAsteroidSpawner
{
    AsteroidSpamPosition GetNewRandomPosition();
}

[System.Flags]
public enum AsteroidSpamPosition
{
    None = 0,
    Left = 1,
    Middle = 2,
    Right = 4
}