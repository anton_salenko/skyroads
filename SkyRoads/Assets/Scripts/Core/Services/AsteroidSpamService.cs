﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AsteroidSpawnRandomService : IAsteroidSpawner
{
    //
    //  can't be used for multi-roads if using same instance of this object
    //  must be injected in different objects for multi-usage
    //
    private AsteroidSpamPosition lastAsteroidsPath = AsteroidSpamPosition.Left;
    private AsteroidSpamPosition sideWay = AsteroidSpamPosition.Left | AsteroidSpamPosition.Middle;
    private int sameWayCounter = 0;

    public AsteroidSpamPosition GetNewRandomPosition()
    {
        AsteroidSpamPosition newPositions = AsteroidSpamPosition.None;
        AsteroidSpamPosition path = AsteroidSpamPosition.None;
        //check middle path
        if ((lastAsteroidsPath & AsteroidSpamPosition.Middle) == AsteroidSpamPosition.Middle)
        {
            // if moving same way too long chenge direction
            if (sameWayCounter > 3)
            {
                newPositions = sideWay & ~AsteroidSpamPosition.Middle;
                path = ~sideWay;
                sideWay = AsteroidSpamPosition.Middle | path;
                Debug.Log("change way");
            }
            else
            {
                int random = Random.Range(0, 2);
                if (random == 0)
                {
                    newPositions = AsteroidSpamPosition.Right;
                    path = Random.Range(0, 10) == 0 ? AsteroidSpamPosition.Middle : AsteroidSpamPosition.Left;

                }
                else
                {
                    newPositions = AsteroidSpamPosition.Left;
                    path = Random.Range(0, 10) == 0 ? AsteroidSpamPosition.Middle : AsteroidSpamPosition.Right;
                }
                if ((path & sideWay) > 0)
                {
                    sameWayCounter++;
                }
                else
                {
                    // if path chenged switch sideway
                    sameWayCounter = 0;
                    sideWay = path | AsteroidSpamPosition.Middle;
                }
            }
            

        }
        else
        {
            // inverce last path and randomly move middle
            newPositions = ~lastAsteroidsPath;
            if (Random.Range(0, 2) != 0)
            {
                newPositions &= ~AsteroidSpamPosition.Middle;
                path = AsteroidSpamPosition.Middle;
            }
            else
            {
                path = lastAsteroidsPath;
            }
        }

        lastAsteroidsPath = path;
        Debug.Log(path);
        return newPositions;
    }
}
