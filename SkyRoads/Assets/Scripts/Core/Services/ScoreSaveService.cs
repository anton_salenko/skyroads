﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IScoreSave
{
    int maxScore { get; }
    bool SetScore(int score);
}

public class PlayerPrefsScoreSaveService : IScoreSave
{
    public int maxScore { get; private set; } 

    public PlayerPrefsScoreSaveService()
    {
        maxScore = PlayerPrefs.GetInt("maxScore", 0);
    }

    public bool SetScore(int score)
    {
        if (score > maxScore)
        {
            maxScore = score;
            PlayerPrefs.SetInt("maxScore", score);
            return true;
        }
        return false;
    }

    
}
