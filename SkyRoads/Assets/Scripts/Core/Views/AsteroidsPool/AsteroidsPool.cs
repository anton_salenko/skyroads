﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AsteroidsPool : BaseView
{

    [SerializeField] ClearObjectsPositionBehaviour clearPosition;
    [SerializeField] GameObject asteroidTemplate;
    [SerializeField] ObjectSpawner objectSpawner;
    [SerializeField] int poolSize = 50;

    

    private Queue<GameObject> pool = new Queue<GameObject>();
    private Transform lastObject = null;


    public void OnStartGame()
    {
        FillObjects();
    }

    public void OnRestartGame()
    {
        ClearAllObjects();
        FillObjects();
    }

    public void OnEndGame()
    {

    }

    private void Update()
    {
        if (lastObject != null && lastObject.position.z < clearPosition.clearPosition)
        {
            ClearLastObject();
            if (pool.Count < poolSize)
            {
                var positions = objectSpawner.GetNewAsteroidPositions();
                foreach (var position in positions)
                {
                    SpawnObject(position);
                }
            }
        }
    }

    private void FillObjects()
    {
        while(pool.Count < poolSize)
        {
            var positions = objectSpawner.GetNewAsteroidPositions();
            foreach (var position in positions)
            {
                SpawnObject(position);
            }
        }
    }

    private void ClearAllObjects()
    {
        if (lastObject != null)
        {
            Destroy(lastObject.gameObject);
            lastObject = null;
        }
        while (pool.Count > 0)
        {
            var go = pool.Dequeue();
            go.SetActive(false);
            Destroy(go);
        }
    }

    private void ClearLastObject()
    {
        if (lastObject == null)
        {
            return;
        }
        Destroy(lastObject.gameObject);
        if (pool.Count > 0)
        {
            lastObject = pool.Dequeue().transform;
        }
    }

    private void SpawnObject(Vector3 position)
    {
        var go = Instantiate(asteroidTemplate, position, Quaternion.identity, transform);
        go.SetActive(true);
        if (lastObject == null)
        {
            lastObject = go.transform;
        }
        else
        {
            pool.Enqueue(go);
        }
    }

    
    

}