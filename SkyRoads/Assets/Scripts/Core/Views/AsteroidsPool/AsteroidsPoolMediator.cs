﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AsteroidsPoolMediator : BaseMediator
{
    [Inject] public AsteroidsPool view { get; set; }

    public override void OnRegister()
    {
        dispatcher.AddListener(Events.GameStart, view.OnStartGame);
        dispatcher.AddListener(Events.GameRestart, view.OnRestartGame);
        dispatcher.AddListener(Events.GameEnd, view.OnEndGame);
    }

    public override void OnRemove()
    {
        dispatcher.RemoveListener(Events.GameStart, view.OnStartGame);
        dispatcher.RemoveListener(Events.GameRestart, view.OnRestartGame);
        dispatcher.RemoveListener(Events.GameEnd, view.OnEndGame);
    }
}
