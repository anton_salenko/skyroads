﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClearObjectsPositionBehaviour : MonoBehaviour
{

    public float clearPosition { get; private set; } = 0f;

    void Update()
    {
        clearPosition = transform.position.z;
    }
}
