﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameMediator : BaseMediator
{
    [Inject] public GameView view { get; set; }

    public override void OnRegister()
    {
        view.LoadView();
        dispatcher.AddListener(Events.ShipHit, view.OnShipHit);
        dispatcher.AddListener(Events.AddScore, view.AddScore);
        dispatcher.AddListener(Events.BoostEnabled, view.OnBoost);
    }

    public override void OnRemove()
    {
        view.RemoveView();
        dispatcher.RemoveListener(Events.ShipHit, view.OnShipHit);
        dispatcher.RemoveListener(Events.AddScore, view.AddScore);
        dispatcher.RemoveListener(Events.BoostEnabled, view.OnBoost);

    }
}
