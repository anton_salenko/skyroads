﻿using strange.extensions.dispatcher.eventdispatcher.api;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameView : BaseView
{
    [Inject] public IScoreSave scoreSaveService { get; set; }

    [SerializeField] Text scoreText;
    [SerializeField] GameObject pressAnyKeyText;
    [SerializeField] GameObject congratsText;
    [SerializeField] Text timePlayedText;
    [SerializeField] Text asteroidPassedText;
    [SerializeField] GameObject gameOverStats;
     
    private Coroutine restartCoroutine = null;
    private bool restarting = false;
    private Coroutine scoreCalculateCoroutine = null;
    private float score = 0f;
    private float scoreMultiplier = 1f;
    private int asteroidPassed = 0;
    private float startTime = 0f;

    public void LoadView()
    {
        StartCoroutine(WaitFrameAndStartGame());
    }

    public void RemoveView()
    {
        if (restartCoroutine != null)
        {
            StopCoroutine(restartCoroutine);
        }
    }

    private void Update()
    {

    }

    public void OnShipHit()
    {
        if (restarting)
        {
            return;
        }
        restartCoroutine = StartCoroutine(RestartCoroutine());
    }

    public void AddScore(IEvent eventData)
    {
        if (restarting)
        {
            return;
        }
        asteroidPassed++;
        var value = (float)eventData.data;
        score += value;
    }

    public void OnBoost(IEvent eventData)
    {
        scoreMultiplier = (bool)eventData.data ? 2f : 1f;
    }

    private IEnumerator ScoreCalculateCoroutine()
    {
        score = 0f;
        scoreText.text = Mathf.FloorToInt(score).ToString();
        while (true)
        {
            score += Time.deltaTime * scoreMultiplier;
            scoreText.text = Mathf.FloorToInt(score).ToString();
            yield return null;
        }
    }

    private IEnumerator RestartCoroutine()
    {
        restarting = true;

        congratsText.SetActive(scoreSaveService.SetScore(Mathf.FloorToInt(score)));
        if (scoreCalculateCoroutine != null)
        {
            StopCoroutine(scoreCalculateCoroutine);
        }
        GameOverUIEnable();
        yield return new WaitForSeconds(3f);
        congratsText.SetActive(false);
        pressAnyKeyText.SetActive(true);
        yield return new WaitUntil(() => Input.anyKeyDown);
        GameOverUIDisable();
        RestartGame();
        restarting = false;
    }

    private void GameOverUIEnable()
    {
        timePlayedText.text = Mathf.FloorToInt(Time.time - startTime).ToString();
        asteroidPassedText.text = asteroidPassed.ToString();
        gameOverStats.SetActive(true);
    }

    private void GameOverUIDisable()
    {
        pressAnyKeyText.SetActive(false);
        gameOverStats.SetActive(false);
    }

    private void RestartGame()
    {
        asteroidPassed = 0;
        startTime = Time.time;
        scoreCalculateCoroutine = StartCoroutine(ScoreCalculateCoroutine());
        dispatcher.Dispatch(Events.ResetSpawner);
        dispatcher.Dispatch(Events.GameRestart);
    }

    private IEnumerator WaitFrameAndStartGame()
    {
        dispatcher.Dispatch(Events.ResetSpawner);
        yield return null;
        pressAnyKeyText.SetActive(true);
        dispatcher.Dispatch(Events.GameLoadLevel);
        yield return new WaitUntil(() => Input.anyKeyDown);
        startTime = Time.time;
        pressAnyKeyText.SetActive(false);
        scoreCalculateCoroutine = StartCoroutine(ScoreCalculateCoroutine());
        dispatcher.Dispatch(Events.GameStart);
    }

    private void EndGame()
    {
        dispatcher.Dispatch(Events.GameEnd);
    }
}
