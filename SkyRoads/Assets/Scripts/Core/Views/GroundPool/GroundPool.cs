﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundPool : BaseView
{
    [SerializeField] ClearObjectsPositionBehaviour clearPosition;
    [SerializeField] GameObject planeTemplate;
    [SerializeField] float startPosition;
    [SerializeField] float objectSize;

    [SerializeField] int poolSize = 50;

    private float lastPosition;
    private Queue<GameObject> planePool = new Queue<GameObject>();
    private Transform lastObject = null;

    public void OnLoadGame()
    {
        lastPosition = startPosition;
        FillObjects();
    }

    public void OnRestartGame()
    {
        ClearAllObjects();
        lastPosition = startPosition;
        FillObjects();
    }

    public void OnEndGame()
    {

    }

    private void Update()
    {
        if (lastObject != null && lastObject.position.z < clearPosition.clearPosition)
        {
            ClearLastObject();
            lastPosition += objectSize;
            SpawnObject(Vector3.forward * lastPosition);
        }
    }

    private void ClearLastObject()
    {
        if (lastObject == null)
        {
            return;
        }
        Destroy(lastObject.gameObject);
        if (planePool.Count > 0)
        {
            lastObject = planePool.Dequeue().transform;
        }
    }

    private void FillObjects()
    {
        while (planePool.Count < poolSize)
        {
            lastPosition += objectSize;
            SpawnObject(Vector3.forward * lastPosition);
        }
        lastObject = planePool.Dequeue().transform;
    }

    private void ClearAllObjects()
    {
        if (lastObject != null)
        {
            Destroy(lastObject.gameObject);
        }
        while (planePool.Count > 0)
        {
            var go = planePool.Dequeue();
            go.SetActive(false);
            Destroy(go);
        }
    }

    private void SpawnObject(Vector3 position)
    {
        var go = Instantiate(planeTemplate, position, Quaternion.identity, transform);
        go.SetActive(true);
        if (lastObject == null)
        {
            lastObject = go.transform;
        }
        else
        {
            planePool.Enqueue(go);
        }
    }
    

}
