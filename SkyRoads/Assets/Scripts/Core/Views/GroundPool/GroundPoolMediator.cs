﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundPoolMediator : BaseMediator
{
    [Inject] public GroundPool view { get; set; }

    public override void OnRegister()
    {
        dispatcher.AddListener(Events.GameLoadLevel, view.OnLoadGame);
        dispatcher.AddListener(Events.GameRestart, view.OnRestartGame);
        dispatcher.AddListener(Events.GameEnd, view.OnEndGame);
    }

    public override void OnRemove()
    {
        dispatcher.RemoveListener(Events.GameLoadLevel, view.OnLoadGame);
        dispatcher.RemoveListener(Events.GameRestart, view.OnRestartGame);
        dispatcher.RemoveListener(Events.GameEnd, view.OnEndGame);
    }
}
