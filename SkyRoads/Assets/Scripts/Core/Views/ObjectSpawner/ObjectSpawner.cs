﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// needed if there will be not only asteroids but another type of interactable obects to spawn

public class ObjectSpawner : BaseView
{
    [Inject] public IAsteroidSpawner spawner { get; set; }

    [SerializeField] float startsSpawnPosition;
    [SerializeField] float minSpawnDistance;
    [SerializeField] float maxSpawnDistance;
    [SerializeField] float timeSpawnInfluence = 60f;
    [SerializeField] float sideSpawnPosition = 3f;
    [SerializeField] float horizontalRandomPosition = 1f;
    private float startGameTime = 0f;
    private float lastSpawnPosition = 0f;
    private AsteroidSpamPosition lastSpam = AsteroidSpamPosition.None;

    public void ResetSpawner()
    {
        startGameTime = Time.time;
        lastSpawnPosition = startsSpawnPosition;
    }

    public List<Vector3> GetNewAsteroidPositions()
    {
        var positionsList = new List<Vector3>();
        var newPositions = spawner.GetNewRandomPosition();
        lastSpam = newPositions;
        lastSpawnPosition += Mathf.Pow(0.5f, (Time.time - startGameTime) / timeSpawnInfluence) * (maxSpawnDistance - minSpawnDistance) + minSpawnDistance;
        if ((newPositions & AsteroidSpamPosition.Middle) == AsteroidSpamPosition.Middle)
        {
            positionsList.Add(new Vector3(Random.Range(-horizontalRandomPosition, horizontalRandomPosition), 2f, lastSpawnPosition));
        }

        if ((newPositions & AsteroidSpamPosition.Right) == AsteroidSpamPosition.Right)
        {
            positionsList.Add(new Vector3(Random.Range(-horizontalRandomPosition, horizontalRandomPosition) + sideSpawnPosition, 2f, lastSpawnPosition));
        }

        if ((newPositions & AsteroidSpamPosition.Left) == AsteroidSpamPosition.Left)
        {
            positionsList.Add(new Vector3(Random.Range(-horizontalRandomPosition, horizontalRandomPosition) - sideSpawnPosition, 2f, lastSpawnPosition));
        }
        return positionsList;
    }


}
