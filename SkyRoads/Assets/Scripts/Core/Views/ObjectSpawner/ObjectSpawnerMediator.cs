﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectSpawnerMediator : BaseMediator
{

    [Inject] public ObjectSpawner view { get; set; }

    public override void OnRegister()
    {
        dispatcher.AddListener(Events.ResetSpawner, view.ResetSpawner);
    }

    public override void OnRemove()
    {
        dispatcher.RemoveListener(Events.ResetSpawner, view.ResetSpawner);
    }
}
