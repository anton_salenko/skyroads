﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreCalculatorView : BaseView
{
    private bool scoreCalculaded = false;


    private void OnTriggerEnter(Collider other)
    {

        if (!scoreCalculaded && other.gameObject.CompareTag("scoreCalculator"))
        {
            dispatcher.Dispatch(Events.AddScore, 5f);
            scoreCalculaded = true;

        }
    }
}
