﻿using strange.extensions.dispatcher.eventdispatcher.api;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipView : BaseView
{

    [SerializeField] float maxHorizontalVelocity;
    [SerializeField] Vector3 startPosition;
    [SerializeField] float horizontalAcceleration = 3f;
    [SerializeField] float forwardVelocity = 5f;
    [SerializeField] GameObject explosionFX;
    [SerializeField] float horizontalCorner = 5f;
    [SerializeField] float maxSpeedBoost = 2f;
    [SerializeField] float forwardVelocityMultiplierAcceleration = 2f;
    [SerializeField] float regularCameraDistance = 10f;
    [SerializeField] float boostCameraDistance = 5f;

    private Rigidbody rigidbody;
    private SmoothFollow cameraFolower;
    private bool gamestarted = false;
    private float forwardVelocityMultiplier = 1f;
    private float movementDirection = 0f;
    private bool move = false;
    private float boost = -1f;
    

    protected override void Start()
    {
        base.Start();
        cameraFolower = Camera.main.GetComponent<SmoothFollow>();
        if (cameraFolower != null)
        {
            cameraFolower.target = transform;
        }
        else
        {
            Debug.LogError("cameraFolower is null");
        }
        rigidbody = GetComponent<Rigidbody>();
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("asteroid"))
        {
            dispatcher.Dispatch(Events.ShipHit);
            move = false;
            explosionFX.SetActive(true);
            rigidbody.velocity = Vector3.zero;
            rigidbody.angularVelocity = Vector3.zero;
            forwardVelocityMultiplier = 1f;
        }
    }

    public void OnStartGame()
    {
        move = true;
        explosionFX.SetActive(false);
    }

    public void OnEndGame()
    {

    }

    public void OnRestartGame()
    {
        move = true;
        explosionFX.SetActive(false);
        transform.position = startPosition;
        transform.rotation = Quaternion.identity;
        rigidbody.velocity = Vector3.zero;
        rigidbody.angularVelocity = Vector3.zero;
    }

    private void Update()
    {
        UpdateInput();
        if (move)
        {
            cameraFolower.distance = Mathf.Lerp(regularCameraDistance, boostCameraDistance, (forwardVelocityMultiplier - 1f) / (maxSpeedBoost - 1f));
        }
    }

    private void UpdateInput()
    {
        movementDirection = 0f;
        if (Input.GetKey(KeyCode.A))
        {
            movementDirection -= 1f;
        }
        if (Input.GetKey(KeyCode.D))
        {
            movementDirection += 1f;
        }
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            movementDirection = Mathf.Clamp(movementDirection - 1f, -1f, 1f);
        }
        if (Input.GetKey(KeyCode.RightArrow))
        {
            movementDirection = Mathf.Clamp(movementDirection + 1f, -1f, 1f);
        }
        if (Input.GetKey(KeyCode.Space))
        {
            boost = 1f;
        }
        else
        {
            boost = -1f;
        }
        if (Input.GetKeyDown(KeyCode.Space))
        {
            dispatcher.Dispatch(Events.BoostEnabled, true);
        }
        if (Input.GetKeyUp(KeyCode.Space))
        {
            dispatcher.Dispatch(Events.BoostEnabled, false);
        }
    }

    private void FixedUpdate()
    {
        if (!move)
        {
            return;
        }
        forwardVelocityMultiplier = Mathf.Clamp(forwardVelocityMultiplier + boost * Time.fixedDeltaTime * forwardVelocityMultiplierAcceleration, 1f, maxSpeedBoost);
        float horizontalVelocity = 0f;

        if (Mathf.Abs(movementDirection) < float.Epsilon)
        {
            horizontalVelocity = Mathf.MoveTowards(rigidbody.velocity.x, 0f, horizontalAcceleration * Time.fixedDeltaTime);
        }
        else
        {
            horizontalVelocity = Mathf.Clamp(rigidbody.velocity.x + movementDirection * horizontalAcceleration * forwardVelocityMultiplier * (maxHorizontalVelocity - rigidbody.velocity.x * movementDirection) / maxHorizontalVelocity * Time.fixedDeltaTime, -maxHorizontalVelocity, maxHorizontalVelocity);
        }

        if (Mathf.Abs(rigidbody.position.x) + Mathf.Epsilon > horizontalCorner)
        {
            horizontalVelocity = 0f;
        }
        float fwdVelocity = forwardVelocity * forwardVelocityMultiplier;
        rigidbody.velocity = new Vector3(horizontalVelocity, 0f, fwdVelocity);
        rigidbody.position = new Vector3(Mathf.Clamp(rigidbody.position.x, -horizontalCorner, horizontalCorner), rigidbody.position.y, rigidbody.position.z);
        rigidbody.rotation = Quaternion.Slerp(rigidbody.rotation, Quaternion.LookRotation(rigidbody.velocity.normalized, Vector3.up), 0.2f);
    }

}
