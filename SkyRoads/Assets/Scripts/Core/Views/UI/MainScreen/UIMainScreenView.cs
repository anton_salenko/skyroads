﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIMainScreenView : BaseView
{
    [Inject]
    public SFXManager sfxManager { get; set; }

    [Inject]
    public IScoreSave scoreSave { get; set; }

    [SerializeField] Button playButton;
    [SerializeField] Toggle musicToggle;
    [SerializeField] Text maxScoreText;
    
    public void LoadView()
    {
        musicToggle.isOn = sfxManager.MusicVolume > float.Epsilon;
        playButton.onClick.AddListener(OnPlayButtonClick);
        musicToggle.onValueChanged.AddListener(OnMusicToggle);
        maxScoreText.text = string.Format("Max score: {0}", scoreSave.maxScore.ToString());
    }

    public void RemoveView()
    {

    }

    private void OnPlayButtonClick()
    {
        dispatcher.Dispatch(Events.GameLoad);
        dispatcher.Dispatch(Events.UIMainScreenRemove);
    }

    private void OnMusicToggle(bool value)
    {
        sfxManager.SetMusic(value);
        dispatcher.Dispatch(Events.SFXOnVolumeChanged);
    }
}
